// ==UserScript==
// @id             bannergress-missions-addon
// @name           IITC plugin: Bannergress missions add-on
// @category       Misc
// @version        0.1.0
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @description    Bannergress missions add-on
// @match          https://intel.ingress.com/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {

    // The reason why this plugin is littered with "const PLUGIN = this" nonsense is because
    // there are other plugins that re-write the missions plugin in ways that cause the
    // hooked methods to lose their scope, so that any variables or things we set around
    // it will be lost 
    //
    // let str = window.plugin.missions.xxx.toString();
    // (replace stuff in str)
    // eval("window.plugin.missions.xxx = " + str)

    function decodeWaypoint(data) {
        var result = {
            hidden: data[0],
            guid: data[1],
            title: data[2],
            typeNum: data[3],
            type: [null, 'Portal', 'Field Trip'][data[3]],
            objectiveNum: data[4],
            objective: [null, 'Hack this Portal', 'Capture or Upgrade Portal', 'Create Link from Portal', 'Create Field from Portal', 'Install a Mod on this Portal', 'Take a Photo', 'View this Field Trip Waypoint', 'Enter the Passphrase'][data[4]],
            portal: undefined
        };
        if (result.typeNum === 1 && data[5]) {
            if (window.decodeArray.portal) result.portal = window.decodeArray.portal(data[5], 'summary'); // IITC-CE 0.31.1 and after
            else result.portal = window.decodeArray.portalSummary(data[5]); // IITC-CE 0.31.1 and below
            // Portal waypoints have the same guid as the respective portal.
            result.portal.guid = result.guid;
        } else if (result.typeNum == 2 && data[5]) { // field trip!
            result.portal = {
                // data[5] = [ "f", <latE6>, <lngE6> ]
                latE6: data[5][1],
                lngE6: data[5][2],
                title: result.title
            }
        }
        return result;
    };
    
    function decodeMission(data) {
        return {
            guid: data[0],
            title: data[1],
            description: data[2],
            authorNickname: data[3],
            authorTeam: data[4],
            ratingE6: data[5],
            medianCompletionTimeMs: data[6],
            numUniqueCompletedPlayers: data[7],
            typeNum: data[8],
            type: [null, 'Sequential', 'Non Sequential', 'Hidden'][data[8]],
            waypoints: data[9].map(decodeWaypoint),
            image: data[10]
        };
    };

    // ensure plugin framework is there, even if iitc is not yet loaded
    if (typeof window.plugin !== 'function') window.plugin = function () { };

    // PLUGIN START ////////////////////////////////////////////////////////

    // use own namespace for plugin
    const PLUGIN = window.plugin.bannergress = function () { };

    PLUGIN.stopBatch = false;

    PLUGIN.setupCSS = function() {

        $('head').append(`<style type="text/css">

        .bannergress-util-disable {
            filter: blur(2px);
            pointer-events: none;
        }

        details.bannersgress-filters summary::before { 
            content:"⊞ "; 
        }

        details.bannersgress-filters[open] summary::before { 
            content:"⊟ "; 
        }

        .bannergress-mission-row {
        }

        .bannergress-mission-extend-title {
            flex: 1;
            display: inline-block;
        }

        .bannergress-mission-row-indexed {
            // background-color: rgba(0,255,0,0.2);
        }
        .bannergress-mission-row-new {
            // background-color: rgba(255,255,0,0.2);
        }
        .bannergress-mission-row-pending {
            // background-color: rgba(255,128,0,0.2);
        }
        .bannergress-mission-row-index-locked {
            // background-color: rgba(0,255,0,0.1);
        }

        .bannergress-mission-status {
            position: absolute;
            right: 0;
            top: 0.25em;
            padding: 0.25em;
            color: white;
            flex: 0;
        }

        .bannergress-mission-status-indexed {
            color: green;
            cursor: not-allowed;
        }

        .bannergress-mission-status-indexed-refresh {
            color: greenyellow;
            cursor: pointer;
        }

        .bannergress-mission-status-pending {
            color: orange;
            cursor: wait;
        }

        .bannergress-mission-status-new {
            color: gold;
            cursor: pointer;
        }

        </style>`);

    }

    PLUGIN.replaceResult = function(old_m, m) {

        const PLUGIN = window.plugin.bannergress;

        // save new
        m.mission = old_m.mission;
        m.elem = old_m.elem;
    
        // replace in results array
        PLUGIN.results = PLUGIN.results.map(function(r) {
            return r == old_m ? m : r;
        })
    
        PLUGIN.updateStatus(m);
    }
    
    PLUGIN.checkData = function(detailOrSummaries) { // returns an AJAX promise

        const PLUGIN = window.plugin.bannergress;

        // let missionIds = detailOrSummaries instanceof Array
        //     ? detailOrSummaries.map(function(m) { return m.guid })
        //     : [ detailOrSummaries.guid ];
        // let checkUrl = PLUGIN.baseUrl + 'missions/guids/' + missionIds.join(",");
        // console.debug(checkUrl);
        // return $.ajax({
        //     type: 'GET',
        //     contentType: "application/json; charset=utf-8",
        //     dataType: "json",
        //     url: checkUrl
        // });

        let submitUrl = PLUGIN.baseUrl + 'missions?format=missionsplugin';
        return $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: submitUrl,
            data: JSON.stringify(detailOrSummaries)
        });

    }

    PLUGIN.isIndexed = function(m) {
        return (m.waypoints || m.detailsModified);
    }

    PLUGIN.getStatus = function(m) {
        const PLUGIN = window.plugin.bannergress;

        if (m && m.pending) {
            return { id: 'pending', icon: '⏳', text: 'Updating', title: 'Updating...', locked: true };
        } else if (m && (m.waypoints || m.detailsModified)) {
            // known and indexed - check if it was done recently
            let lockTime = 15 * 1000 ; // 1000 * 60 * 60 * 24 * 7; // 7 days
            let lockedUntil = m.detailsModified + lockTime;
            let now = Date.now();
            let deltaTime = lockedUntil - now;
            let time = new Date(m.detailsModified).toLocaleDateString();
            if (deltaTime > 0) {
                console.log("MODIFIED", m.detailsModified, lockTime, deltaTime, time);
                let DAYMILLIS = 24 * 60 * 60 * 1000;
                let HOURMILLIS = 60 * 60 * 1000;
                let MINUTEMILLIS = 60 * 1000;
                let days = Math.floor(deltaTime / DAYMILLIS);
                let hours = Math.floor((deltaTime % DAYMILLIS) / HOURMILLIS);
                let mins = Math.ceil((deltaTime % HOURMILLIS) / MINUTEMILLIS);
                
                let lockedTime = days+"d " + hours+"h " + mins+"m";
                return { id: 'indexed', icon: '🔒', text: 'Indexed', locked: true, title: 'Last updated in Bannergress: ' + time + ' - You can update again in ' + lockedTime}
            } else {
                return { id: 'indexed-refresh', icon: '✅', text: 'Indexed', title: 'Last updated in Bannergress: ' + time + ' - click to refresh!' }
            }
        } else if (m) {
            // known, but no details
            return { id: 'new', icon: '🔃', text: 'Not indexed', title: 'Click to add to bannergress!' }
        } else {
            // not known
            return { id: 'new', icon: '🔃', text: 'Not indexed', title: 'Click to add to Bannergress!' }
        }
    }

    PLUGIN.makeElem = function(status) {
        const PLUGIN = window.plugin.bannergress;
        let ours = $('<span title="' + status.title + '" class="bannergress-mission-status bannergress-mission-status-' + status.id + '">' + status.icon + '</span>');
        return ours;
    }

    PLUGIN.updateStatus = function(m, status) {
        const PLUGIN = window.plugin.bannergress;

        if (status == null) status = PLUGIN.getStatus(m);
        let newElem = $(PLUGIN.makeElem(status));
        m.elem.replaceWith(newElem);
        m.elem = newElem;
    }

    PLUGIN.downloadMission = function(m, callback) {
        // update state and redraw ui element to "pending"
        m.pending = true;
        PLUGIN.updateStatus(m);

        console.log("LOADING MISSION:", m.mission.guid);
        window.plugin.missions.loadMission(m.mission.guid,
            function(details) {

                console.log("MISSION LOADED:", details);

                // we need to check if there are fieldtrip waypoints in the mission, because the missions plug-in does not give us lat/lngs for it
                let ok = true;
                let saveType = "missionsplugin";
                if (details.waypoints) {
                    details.waypoints.forEach(function(w) {
                        if (w.typeNum == 2 && w.portal == null) { // fieldtrip 
                            ok = false;
                        }
                    });
                }

                const save = function() {
                    console.log("SUBMITTING TO BACKEND..");
                    setTimeout(() => {

                        let submitUrl = PLUGIN.baseUrl + 'missions?format=' + saveType;

                        let promise = $.ajax({
                            type: 'POST',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            url: submitUrl,
                            data: JSON.stringify(details)
                        });

                        promise.then(function(results) {

                            console.log("SUBMITTED TO BACKEND:", results);
                            PLUGIN.replaceResult(m, results.results[0]);

                            if (callback) callback(null);

                        }).catch(function(err) {

                            console.error("ERROR SUBMITTING TO BACKEND:", err);

                            // reset
                            m.pending = false;
                            PLUGIN.updateStatus(m);

                            if (callback) callback(err);

                        });

                    }, 500);                                            
                }

                if (ok) save();
                else {
                    console.log("NEED TO RE-QUERY MISSION DETAILS FROM INTEL TO GET FIELDTRIP WAYPOINT LAT/LNGS..");

                    window.postAjax('getMissionDetails', {
                        guid: m.mission.guid
                    }, function(data) {

                        console.log("GOT INTEL DATA", data);
                        data = decodeMission(data.result);
                        console.log("DECODED INTEL DATA", data);
                        save();

                        if (callback) callback(null);

                    }, function(err) {
                        // reset
                        m.pending = false;
                        PLUGIN.updateStatus(m);

                        if (callback) callback(err);
                    });
                }

            },
            function(err) {

                console.error("ERROR QUERYING MISSION DETAILS FROM INTEL:", err);

                // reset
                m.pending = false;
                PLUGIN.updateStatus(m);

                alert("Error loading mission data from Intel or submitting mission to Bannergress backend: " + err);

                if (callback) callback(null);
            }
        );
    }

    PLUGIN.makeClickable = function(m) {
        const PLUGIN = window.plugin.bannergress;

        status = status || PLUGIN.getStatus(m);
        if (!status.locked) {
            m.elem.click(function() {
                console.log("DOWNLOAD", m);
                PLUGIN.downloadMission(m);
            });
        }                    
    }

    PLUGIN.install = function() {

        const PLUGIN = window.plugin.bannergress;
        const MISSIONS_PLUGIN = window.plugin.missions;

        // find the missions plugin
        if (!MISSIONS_PLUGIN) {
            console.error("⚠⚠⚠ MISSIONS PLUG-IN NOT FOUND - BANNERGRESS PLUGIN DISABLED ⚠⚠⚠");
            return;
        }

        // traps for window.dialog()
        PLUGIN.captureDialog = false;
        PLUGIN.capturedDialogs = [];
        PLUGIN.captureDialogBefore = null;
        PLUGIN.dialog = window.dialog;
        window.dialog = function() {
            const PLUGIN = window.plugin.bannergress;
            let args = [];
            for (let i = 0; arguments.length > i; i++) args.push(arguments[i]);
            if (PLUGIN.captureDialogBefore) {
                args = PLUGIN.captureDialogBefore(args);
            }
            let dlg = PLUGIN.dialog.apply(window, args);
            if (PLUGIN.captureDialog) {
                PLUGIN.capturedDialogs.push(dlg);
            }
            return dlg;
        }

        // save original window.plugin.missions.renderMissionSummary() and install our intercept
        PLUGIN.renderMissionSummary = MISSIONS_PLUGIN.renderMissionSummary;
        
        MISSIONS_PLUGIN.renderMissionSummary = function(mission) {

            const PLUGIN = window.plugin.bannergress;
            const MISSIONS_PLUGIN = window.plugin.missions;

            // call original
            let el = PLUGIN.renderMissionSummary.call(window.plugin.missions, mission);
            //console.log("RE-RENDER %o -> %o", mission, el);

            let m = PLUGIN.results.find(function(x) { return x.guid == mission.guid });
            // console.log("match: ", m, PLUGIN.results, mission.guid);

            if (m == null) {
                // this will happen if we're in a single mission dialog, without results having been loaded for this mission
                console.warn("DO NOT KNOW ANYTHING ABOUT THIS MISSION - MIGHT BE SINGLE MISSION DIALOG?", mission);
                m = {
                    pending: true
                }
                PLUGIN.results.push(m); // add temporary

                // check with server..
                PLUGIN.checkData(mission)
                .then(function(results) {
                    console.log("QUERIED BACKEND FOR SINGLE MISSION:", results);
                    PLUGIN.replaceResult(m, results.results[0]);
                }).catch(function(err) {
                    console.error("ERROR QUERYING BACKEND FOR SINGLE MISSION:", err);
                    m.pending = false;
                    PLUGIN.updateStatus(m);
                });
            }

            m.mission = mission;
            let status = PLUGIN.getStatus(m);
            let ours = PLUGIN.makeElem(status);

            //--- add after <a>
            let a = $(el).find("a");
            ours.insertAfter(a);

            //$(el).addClass('bannergress-mission-row');
            //$(el).addClass('bannergress-mission-row-' + status.id); // TODO: this must be removed also!

            if (m) m.elem = ours;

            PLUGIN.makeClickable(m);

            return el;
        };

        // save original window.plugin.missions.showMissionDialog() and install our hook
        PLUGIN.showMissionDialog = MISSIONS_PLUGIN.showMissionDialog;

        MISSIONS_PLUGIN.showMissionDialog = function() {

            const PLUGIN = window.plugin.bannergress;
            const MISSIONS_PLUGIN = window.plugin.missions;

            let args = [];
            for (let i = 0; arguments.length > i; i++) args.push(arguments[i]);
            let mission = args[0];

            console.log("INTERCEPTED MISSION DIALOG:", mission);
            
            let res = PLUGIN.showMissionDialog.apply(MISSIONS_PLUGIN, args);

            // TODO: ?

            return res;
        }

        // save original window.plugin.missions.showMissionListDialog() and install our hook
        PLUGIN.showMissionListDialog = MISSIONS_PLUGIN.showMissionListDialog;

        MISSIONS_PLUGIN.showMissionListDialog = function() {

            const PLUGIN = window.plugin.bannergress;
            const MISSIONS_PLUGIN = window.plugin.missions;

            let args = [];
            for (let i = 0; arguments.length > i; i++) args.push(arguments[i]);
            let missions = args[0];

            console.log("INTERCEPTED MISSION LIST DIALOG:", missions);

            const afterFetch = function() {
                // call the original window.plugin.missions.showMissionListDialog()
                PLUGIN.capturedDialogs = [];
                PLUGIN.captureDialog = true;
                PLUGIN.captureDialogBefore = function(args) {
                    const PLUGIN = window.plugin.bannergress;
                    console.error("DIALOG ARGS ->", args);

                    opts = args[0];

                    // do some tweaks
                    opts.height = Math.round(window.innerHeight * 0.9);
                    if (window.innerWidth > 600) {
                        opts.width = 600;
                    }

                    // limit to one window
                    // FIXME: because PLUGIN.results is global
                    if (!opts.id) opts.id = "missions-in-view-plus-bannergress";

                    return args;
                }
                PLUGIN.showMissionListDialog.apply(MISSIONS_PLUGIN, args);
                setTimeout(function() {
                    PLUGIN.captureDialog = false;
                    PLUGIN.captureDialogBefore = null;
                }, 0);

                if (PLUGIN.capturedDialogs.length) {

                    let dlg = PLUGIN.capturedDialogs[0];
                    console.log("HAVE DIALOG", dlg);

                    // get buttons
                    let buttons = dlg.dialog('option', 'buttons');
                    console.log("buttons -> ", buttons);

                    // add
                    let btn = {
                        text: "Bannergress",
                        click: function() {
                            PLUGIN.showSettingsDialog();
                        }
                    }

                    if (buttons instanceof Array) {
                        console.log("inject in buttons array");
                    } else {
                        console.log("inject in buttons object")

                        // transform to an array
                        buttons = Object.keys(buttons).map(function(key) {
                            return {
                                text: key,
                                click: buttons[key]
                            }
                        });
                    }

                    // inject ours
                    buttons.unshift(btn);

                    // replace
                    dlg.dialog('option', 'buttons', buttons);

                    // add some additional elements (search etc)
                    let div = dlg.find('> div');
                    let elems = $(`
<details style="width: 100%" class="bannersgress-filters" open>
    <summary>Bannergress utilities</summary>
    <div style="width: 100%; padding-bottom: 1em">
        <div class="bannergress-filters-options">
            Filters<br/>
            <div style="display: flex; flext-direction: row">
                <input display="flex: 1" class="bannergress-name-filter" type="text" placeholder="filter by mission name" style="width: 100%">
                <!--<button display="flex: 0; margin-left: 0.5em" class="bannergress-apply-filters">Apply</button>-->
            </div>
            <div style="display: flex; flex-direction: row">
                <input type="checkbox" id="bannergress-unindexed-filter" class="bannergress-unindexed-filter" title="Show only missions not yet indexed in Bannergress"> <label for="bannergress-unindexed-filter">show unindexed only</label>
                &nbsp;
                <input type="checkbox" id="bannergress-sort-filter" class="bannergress-sort-filter" title="Sort results alphabetically"> <label for="bannergress-sort-filter">sort results alphabetically</label>
            </div>
        </div>

        <div class="bannergress-functions" style="padding-top: 1em">
            <button class="bannergress-functions-fetch-all">Fetch all unindexed</button>
        </div>

        <div class="bannergress-batch" style="display: none; padding-top: 1em">            
            <div class="bannergress-batch-status">Processing mission N of M ..</div>
            <div style="display: flex; flex-direction: row">
                <progress class="bannergress-batch-progress" style="flex: 1" max="100" value="70"></progress>
                <button style="margin-left: 0.5em; flex: 0" class="bannergress-batch-stop-button">Stop</button>
            </div>
        </div>

    </div>
</details>
`);
                    elems.insertBefore(div);
                    //div.prepend(elems);
                    let nameFilterInput = elems.find(".bannergress-name-filter");
                    let filterUnindexedCbx = $(".bannergress-unindexed-filter").first();
                    let sortCbx = $(".bannergress-sort-filter").first();
                    console.log("input", nameFilterInput);

                    function getFilteredMissions(forceOnlyUnindexed) {
                        let nameFilter = nameFilterInput.val().toLowerCase();
                        //let missions = PLUGIN.results.map(function(r) { return r.mission });
                        let onlyUnindexed = forceOnlyUnindexed || $(".bannergress-unindexed-filter:checked").length > 0;
                        console.log(onlyUnindexed);
                        let filteredMissions = PLUGIN.results.filter(function(m) {
                            let mission = m.mission;
                            //console.log("filter mission", m, nameFilter, onlyUnindexed);
                            return mission && mission.title.toLowerCase().indexOf(nameFilter) >= 0 && (!onlyUnindexed || !PLUGIN.isIndexed(m))
                        })
                        if ($(".bannergress-sort-filter:checked").length > 0) {
                            filteredMissions.sort(function(a,b) { return a.mission.title.toLowerCase().localeCompare(b.mission.title.toLowerCase()) })
                        }
                        console.dir(filteredMissions);
                        return filteredMissions;
                    }

                    function applyFilters() {
                        let results = getFilteredMissions();
                        let missions = results.map(function(m) { return m.mission });
                        console.log("applyFilters -> ", results, missions);
                        let newDiv = $(MISSIONS_PLUGIN.renderMissionList(missions));
                        $(div).replaceWith(newDiv);
                        div = newDiv;
                    }

                    filterUnindexedCbx.click(function(ev) {
                        applyFilters();
                    })

                    sortCbx.click(function(ev) {
                        applyFilters();
                    })

                    nameFilterInput.on('input', function(ev) {
                        applyFilters();
                    })

                    // nameFilterInput.keydown(function(ev) {
                    //     if (ev.key == "Enter" || ev.keyCode == 13) {
                    //         applyFilters();
                    //     }
                    // })

                    PLUGIN.stopBatch = false;

                    elems.find(".bannergress-batch-stop-button").first().click(function(ev) {
                        PLUGIN.stopBatch = true;
                    });

                    elems.find(".bannergress-functions-fetch-all").first().click(function(ev) {

                        PLUGIN.stopBatch = false;
                        let filteredMissions = getFilteredMissions(true);

                        // TODO: maybe implement a cut-off at some number here, e.g. 50, or 100

                        if (filteredMissions.length > 0) {
                            if (confirm(`This will fetch ${filteredMissions.length} mission${filteredMissions.length != 1 ? 's' : ''} - are you sure you want to continue?`)) {
                                let funs = elems.find(".bannergress-functions");
                                console.log(funs);
                                elems.find(".bannergress-functions").hide();
                                elems.find(".bannergress-batch").show();
                                let num = 0;

                                let progress = elems.find(".bannergress-batch-progress").first();
                                let status = elems.find(".bannergress-batch-status").first();
                                let count = filteredMissions.length;
                                let okCount = 0;
                                let errCount = 0;
                                let failed = [];

                                $(".bannergress-filters-options").addClass("bannergress-util-disable");
                                div.addClass("bannergress-util-disable");

                                function next() {
                                    status.text("");
                                    let cur = filteredMissions.shift();
                                    console.log("cur", cur);
                                    if (cur && !PLUGIN.stopBatch) {
                                        let percent = Math.round(100 * num / count);
                                        console.log("batch", num, percent);
                                        status.text(`Processing ${num+1} of ${count}..`);
                                        progress.val(percent);
                                        num++;

                                        PLUGIN.downloadMission(cur, function(err) {
                                            if (err) {
                                                errCount++;
                                                failed.push(cur);
                                            } else {
                                                okCount++;
                                            }
                                            let wait = filteredMissions.length > 0 ? Math.round(1000 + Math.random() * 2000) : 0;
                                            setTimeout(() => next(), wait); // random waiting
                                        });

                                    } else {
                                        div.removeClass("bannergress-util-disable");
                                        $(".bannergress-filters-options").removeClass("bannergress-util-disable");
                                        elems.find(".bannergress-functions").show();
                                        elems.find(".bannergress-batch").hide();
                                    }
                                }
                                next();

                            }
                        }
                    })

                    elems.find(".bannergress-apply-filters").click(function(ev) {
                        applyFilters();
                    });
                }
            }

            PLUGIN.checkData(missions)            
            .then(function(results) {
                console.log("QUERIED BACKEND:", results);
                PLUGIN.results = results.results;
                afterFetch();
            })
            .catch(function(err) {
                console.error("ERROR QUERYING BACKEND:", err);                
                PLUGIN.results = [];
                afterFetch();
            });

            // let dlg = $(".ui-dialog[aria-describedby='dialog-plugin-mission-view-dialog']");
            // if (dlg) {
            //     // get buttons
            //     let buttons = dlg.dialog('option', 'buttons');
            //     console.log("buttons -> ", buttons);
            //     let btn = {
            //         text: "HELLO"
            //     }
            //     if (buttons instanceof Array) buttons.push(btn);
            //     else buttons._bannergress = btn;
            // }

        }

        PLUGIN.showSettingsDialog = function() {
            dialog({
                id: "bannergress-settings-dialog",
                title: "Bannergress settings",
                html: `<div><button class="bannergress-login-button">Log in</button></div>`,
                focusCallback: () => {

                    $(".bannergress-login-button").click(function() {

                        let keycloakConfig = {
                            "realm": "bannergress-test",
                            "auth-server-url": "https://login.bannergress.com/auth/",
                            "ssl-required": "external",
                            "resource": "bannergress-iitc-plugin",
                            "public-client": true,
                            "confidential-port": 0
                        };

                        let keycloakConfigUrl = "data:application/json;base64," + btoa(JSON.stringify(keycloakConfig));

                        console.log("creating keycloak interface..")
                        let keycloak = new Keycloak(keycloakConfigUrl);

                        console.log("! initializing keycloak..");
                        try {
                            keycloak.init().then(function(authenticated) {
                                alert(authenticated ? 'authenticated' : 'not authenticated');
                                if (!authenticated) {
                                    keycloak.login();
                                }
                            }).catch(function() {
                                alert('failed to initialize');
                            });
                        } catch (err) {
                            console.error("keycloak initialization error:", err);
                        }

                    })

                },
                closeCallback: () => {
                    // TODO
                }        
            });
    
        }

    }

    const setup = function () {

        const PLUGIN = window.plugin.bannergress;

        PLUGIN.setupCSS();

        // wait for IITC to finish loading
        addHook("iitcLoaded", function() {
            console.log("IITC LOADED");

            PLUGIN.results = [];
            PLUGIN.baseUrl = "https://specops.quest/api/";
            //PLUGIN.baseUrl = "http://localhost:8420/api/";

            console.log("loading keycloak script..");

            $.getScript("https://login.bannergress.com/auth/js/keycloak.js").then(function() {
                console.log("keycloak script loaded");

                // There are some plugins that patch various methods we
                // patch - in somewhat weird ways - so let them do their work
                // first, if installed
                setTimeout(function() { PLUGIN.install() }, 100);
            })

        });
    }

    // PLUGIN END //////////////////////////////////////////////////////////

    setup.info = plugin_info; //add the script info data to the function as a property
    if (!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if (window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end

// inject code into site context
const script = document.createElement('script');
const info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);
